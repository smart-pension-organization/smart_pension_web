// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import axios from 'axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Vue from 'vue';
import App from './App';
import router from './router';

Vue.use(ElementUI);

Vue.config.productionTip = false

//设置axios默认发送请求url前缀
axios.defaults.baseURL="http://localhost:9600/"

//创建axios对象
let sender=axios.create({})
//配置请求拦截器(是在请求发送到控制器之前进行一些操作)
sender.interceptors.request.use(
  config =>{
    //获取token
    let token=window.localStorage.getItem("token")
    if(token){
      //有token带上
      config.headers.authorization = token
    }

    let refreshtoken=window.localStorage.getItem("refreshtoken")
    if(refreshtoken){
      config.headers.refreshtoken=refreshtoken
    }
    //放行请求
    return config
  }
)

//配置响应拦截(是在接受到响应之后进行一些操作)
sender.interceptors.response.use(
  response=>{
    //从响应头中获取token和refreshtoken
    let token=response.headers.authorization
    let refreshtoken=response.headers.refreshtoken

    //判空
    if(token){
      //放本地
      window.localStorage.setItem("token",token)
    }
    if(refreshtoken){
      window.localStorage.setItem("refreshtoken",refreshtoken)
    }

    //返回该对象是为了让发送请求的方法能够得到响应的结果
    return response;
  }
)


//将axios注册为vue对象的原属性
//给vue对象动态添加一个属性，属性名叫$axios，指向axios对象
Vue.prototype.$axios=sender

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
