import Centre from '@/page/centre'
import Layout from '@/page/layout'
import Login from '@/page/login'
import Regist from '@/page/regist'
import Order from '@/page/order/order'
// import OrderDetail from '@/page/order/orderdetail'
import Disabled from '@/page/disabled_management/disabled'
import LoginLog from '@/page/system_management/loginlog'
import OperationLog from '@/page/system_management/operationlog'
import Role from '@/page/system_management/role'
import User from '@/page/system_management/user'
import Vue from 'vue'
import Router from 'vue-router'

//获取原型对象上的push函数
const originalPush = Router.prototype.push
//修改原型对象中的push方法
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router) //全局路由注册

let router = new Router({
  routes: [
    {
      path:'/',
      name:Login,
      component:Login
    },
    {
      path:'/regist',
      name:Regist,
      component:Regist
    },
    {
      path:'/',
      name:Layout,
      component:Layout,
      children:[
        {
          path:'/main',
          name:'Centre',
          component:Centre
        },
        {
          path:'order',
          name:'Order',
          component:Order
        },
        {
          path:'order/orderdetail',
          name:'OrderDetail',
          component:() => import('@/page/order/orderdetail')
        },
        {
          path:'user',
          name: "user",
          component:User
        },
        {
          path:'loginlog',
          name: "loginlog",
          component:LoginLog
        },
        {
          path:'operationlog',
          name: "operationlog",
          component:OperationLog
        },
        {
          path:'role',
          name: "role",
          component:Role
        },
        {
          path:'bill/billdetail',
          name:'BillDetail',
          component:()=> import('@/page/bill/billdetail')
        },
        {
          path:'bill',
          name:'Bill',
          component:()=>import('@/page/bill/bill')
        },
        {
          path:'disabled',
          name: "disabled",
          component:Disabled
        },
        {
          path:'disabled_management/disabled_view',
          name:'disabled_view',
          component:() => import('@/page/disabled_management/disabled_view')
        },
        {
          path:'refund',
          name:'Refund',
          component:() => import('@/page/refund/refund')
        },
        {
          path:'refundconfigure',
          name:'RefundConfigure',
          component:() => import('@/page/refund/refundconfigure')
        },
        {
          path:'healthdata',
          name:'healthdata',
          component:() => import('@/page/health_management/healthdata')
        },
        {
          path:'healthevaluation',
          name:'healthevaluation',
          component:() => import('@/page/health_management/healthevaluation')
        },
        {
          path:'healthremark',
          name:'healthremark',
          component:() => import('@/page/health_management/healthremark')
        },
        {
          path:'ad',
          name:'ad',
          component:() => import('@/page/ad_management/ad')
        },
        {
          path:'essay',
          name:'essay',
          component:() => import('@/page/essay_management/essay')
        },
        {
          path:'carersdetail',
          name:'CarersDetail',
          component:() => import('@/page/carers/carersdetail')
        },
        {
          path:'mapcontainer',
          name:'MapContainer',
          component:() => import('@/page/MapContainer/MapContainer')
        },
        {
          path:'orderitem',
          name:'OrderItem',
          component:() => import('@/page/orderitem/orderitem')
        },
        {
          path:'comment',
          name:'comment',
          component:() => import('@/page/comment_management/comment')
        },
     ]
        
             
    },
    // {
    //   path: '/hello',   //定义访问某个页面（组件）的路径 以/开头 ，该/代表项目根路径
    //   name: 'HelloWorld', //路由的名字,可以通过路由名进行跳转
    //   component: HelloWorld //注册页面，组件
    // }
  ],
  mode:"history" //设置路由模式 history模式可以去除url中的#
})
  
export default router
